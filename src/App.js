import { useEffect, useState } from "react";

function App() {
  const [recipe, setRecipe] = useState([]);
  const [name, setName] = useState("");
  const [ingredientList, setIngredientList] = useState(() => {
    const saved = JSON.parse(localStorage.getItem("ingredientList"));
    if(saved) {
      return saved;
    }
    return ["Carottes"];
  });

  function handleAddIngredient() {
    if(name) {
      const ingredientListCopy = ingredientList;
      ingredientListCopy.push(name);
      localStorage.setItem("ingredientList", JSON.stringify(ingredientListCopy));
      setIngredientList([...ingredientListCopy]);
    }
    else {
      window.alert("Name can't be empty");
    }
  }

  function handleNameChange(event) {
    setName(event.target.value);
  }

  function handleImageChange(event) {
    setImage(event.target.value);
  }
  
  function clearList() {
    localStorage.setItem("ingredientList", JSON.stringify([]));
    setIngredientList([]);
  }

  function addIngredientInRecipe(ingredient) {
    const recipeCopy = recipe;
    if(!recipe.includes(ingredient)) {
      recipeCopy.push(ingredient);
      setRecipe([...recipeCopy])
    }
  }

  const [image, setImage] = useState("");

  return (
    <>
      <input type="text" onChange={handleNameChange} value={name}></input>
      <button onClick={handleAddIngredient}>Add ingredient</button>
      <button onClick={clearList}>Clear list</button>
      <h2>Ingredients :</h2>
      {
        ingredientList.map((ingredient) => {  
          return(
              <button onClick={() => addIngredientInRecipe(ingredient)}>Ajouter {ingredient}</button>
          )
        })
      }
      <h2>Recette :</h2>
      {
        recipe.map((ingredient) => {  
          return(
              <li>{ingredient}</li>
          )
        })
      }
      <h2>Photo</h2>
      <input type="text" value={image} onChange={handleImageChange}></input>
      <button >Add Photo</button>
      <img src={image}></img>
    </>
  );
}

export default App;
